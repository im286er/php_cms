<?php
class Page
{
    private $total;   //数据表中总行数
    private $listRows;  //每页显示行数，就是从数据库中每一次取出的条数
    private $pageNum; //总的页数
    private $page;    //url上page
    /*private $limit = "limit 0, 10"; 封装到类中，不要写死, 这个limit
     *本来就是动态的
     */
    private $limit;   
    private $config = array('header' => '记录', 'prev' => '上一页', 'next' => '下一页', 'first' => '首页', 'last' => '尾页');
   /**
       * 分页时浏览器里地址栏上的url是变化的
       * 如何每次刷新获取url成立做分页类的难点
    **/  
    private $url;
    private $listNum = 5;     //列表的数目
    
    public function __construct($total, $listRows = 10)
    {
        $this->total = $total;
        $this->listRows = $listRows;

        //获取url
        $this->url = $this->getUrl();
        $this->pageNum = ceil($this->total / $this->listRows);  //计算总的页数
        //不为空返回，直接用收到的page,为空的话，获得了当前页
        //$this->page = !empty($_GET["page"]) ? $_GET["page"] : 1;
        //$this->page 严格控制其值
        $this->page = $this->getPage();
        $this->limit = $this->setLimit();
       // print_r($this);
    
    }
/**
 * 获取页面page，当前的页数
 **/
    private function getPage()
    {
        if (isset($_GET['page']))
        {
            $page = $_GET['page']; //url没有设置page
        }
        else
        {
            return 1;
        }
        //$page 为空或者不为数字
         if (empty($page) || !is_numeric($page))
         {
            return 1;
         }
         else if ($page > $this->pageNum) //url page页数大于实际页数
         {
            return $this->pageNum;
         }
         else if ($page <= 0)  //url page 页数小于0
         {
            return 1;
         }
         else
         {
            return $page;
         }
        
    }
    private function setLimit()
    {
        return 'limit ' . ($this->page - 1) * $this->listRows . ", {$this->listRows}";
    }

    
    /*limit 是私有的成员，当在类外使用
     *$page = new Page()时，$page->limit时，自动调用调用魔术方法
     *__get()方法，并将其名字limit 传给__get($argu_name)
     * 完成任务，如:
     *自动调用__get 方法*/
    public function __get($args)   
    {                             
    	if ($args == 'limit')
    	{
    		return $this->limit;
    	}
    	else
    	{
    		return null;
    	}
    }

    /**
     * 获取页面每次刷新时的url
     **/
    private function getUrl()
    {   
        /**
            *如果这个url没有问号，就加上问号
            *strpos(), 查找字符串首次出现的位置
            *url是http://localhost/smarty/employeelist.php?id=1&page=2
            *$_SERVER['REQUEST_URI'];
            *输出/smarty/employeelist.php?id=1&page=2
         **/
        $path = $_SERVER['REQUEST_URI']; 
        $url = $path . (strpos($path, '?') ? null : '?');
        /**
            * parse_url()返回url的组成部分比如host，path，query
            * print_r($parse);
            * 输出Array ( [path] => /smarty/employeelist.php [query] => id=1&page=2 ) 
         **/
        $parse = parse_url($url); 

        /**
         * 如果存在$parse['query'], 则表示有查询,就是url?后面的部分
         * 比如http://localhost/smarty/employeelist.php?id=1&page=2
         * print_r($params);
         * 显示Array ( [id] => 1 [page] => 2 ) 
         **/
        if (isset($parse['query']))
        {
            parse_str($parse['query'], $params);
            //page是变化的，删掉后加入新的page
            unset($params['page']);
            /**
                * unset($params['page']);后
                * $params 变成Array ([id] => 1)
                * 如果$str = http_build_query($params);
                * $str 就变成id=1
             **/
             //所以，最终的url
            $url = $parse['path'] . '?' . http_build_query($params);

        }
       return $url; 
    }

    private function count_start()
    {
        if ($this->total == 0)
        {
            return 0;
        }
        else
        {
            /**
             * 计算当前页的第一条是多少条记录, 当前页减1，加1
             **/
            return ($this->page - 1) * $this->listRows + 1;
        }
    }
    private function count_end()
    {
        /**
         * 计算当前页的最后一条记录的顺序数
         **/
        if ($this->total == 0)
        {
            return 0;
        }
        else
        {
            return min($this->page * $this->listRows, $this->total);
        }
    }

    /**
     * 显示第一页
     **/
   private function first_page()
   {
       if ($this->page == 1) //如果当前页是第一页
       {
            $html = '';
       }
       else
       {
            $html = "<a href='{$this->url}&page=1'>{$this->config['first']}</a>";
       }
       return $html;
   }

    //显示最后一页
    private function last_page()
    {
        if ($this->page == $this->pageNum) //如果当前页是最后一页
        {
            $html = '';
        }
        else
        {
            $html = "<a href='{$this->url}&page={$this->pageNum}'>{$this->config['last']}</a>";
        }
        return $html;
    }

    //上一页
    private function prev_page()
    {
        if ($this->page == 1) //如果是第一页就没有上一页
        {
            $html = '';
        }
        else
        {
            $html = "<a href='{$this->url}&page=" . ($this->page - 1) . "'>{$this->config['prev']}</a>";
        }
        return $html;
    }
    
    //下一页
    private function next_page()
    {
        if ($this->page == $this->pageNum) //如果是第最后一页就没有下一页
        {
            $html = '';
        }
        else
        {
            $html = "<a href='{$this->url}&page=" . ($this->page + 1) . "'>{$this->config['next']}</a>";
        }
        return $html;
    }

    /**
     * 列表的显示
     **/
    private function page_list()
    {
        $linkPage = '';
        $inum = floor($this->listNum / 2); // floor(4.5) 4
        for ($i = $inum; $i >= 1; $i--)
        {
           $page = $this->page - $i;
           if ($page < 1) //小于1就结束 
           {
                continue;
           }
           $linkPage .= "&nbsp;<a href='{$this->url}&page={$page}'>$page</a>&nbsp;";
        }
           $linkPage .= "&nbsp;{$this->page}&nbsp;";

        for ($i = 1; $i <= $inum; $i++)
        {
            $page = $this->page + $i;
           if ($page <= $this->pageNum) //如果$page 小于总的页数
           {
                                                         //注意此处的&前没有问号?              
                $linkPage .= "&nbsp;<a href='{$this->url}&page={$page}'>$page</a>&nbsp;";
           }
           else
           {
                //结束循环
               break;
           }
        }
        return $linkPage;
    }
    /**
     * 跳转到某页
     **/
    private function go_page()
    {
        $html = '';
        //一段js脚本
        $script = <<<EOT
        <select size="1" onchange="window.location='$this->url&page=' + this.value">
EOT;
        $html .= $script;
        for ($i = 1; $i <= $this->pageNum; $i++)
        {
            $html .= "<option value='{$i}'>{$i}</option>";
        }
        $html .= '</select>';
        return $html;
    }
    /**
     * 前台显示的分页
     **/
    public function fpage($display = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9))
    {
        $html[0] = "&nbsp;表中共有<b>{$this->total}</b>{$this->config['header']}&nbsp;";
        $html[1] = '&nbsp;本页显示<b>' . ($this->count_end() - $this->count_start() + 1) . '</b>条&nbsp;';
        $html[2] = "&nbsp;本页显示第<b>{$this->count_start()}</b>到<b>{$this->count_end()}</b>条&nbsp;";
        $html[3] = "&nbsp;当前页<b>{$this->page}/</b>共<b>{$this->pageNum}</b>页&nbsp;";

        $html[4] = "&nbsp;{$this->first_page()}&nbsp;";// 首页 
        $html[5] = "&nbsp;{$this->prev_page()}&nbsp;";// 上一页 
        $html[6] = $this->page_list();                //列表 
        $html[7] = "&nbsp;{$this->next_page()}&nbsp;";// 下一页
        $html[8] = "&nbsp;{$this->last_page()}&nbsp;";// 尾页 
        $html[9] = "&nbsp;到{$this->go_page()}页"; // 跳转
        
        $fpage = '';
        foreach ($display as $index)
        {
            $fpage .= $html[$index];
        }
        return $fpage;
    }
}
?>
