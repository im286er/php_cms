<?php
require('./image.class.php');
$image = new Image('./');    //要处理的图片的文件夹

//对图片进行缩放
echo $image->thumb('fire.jpg', 200, 200, 'th1_');  //返回新的文件名
echo $image->thumb('fire.jpg', 300, 300, 'th2_');  //返回新的文件名
echo $image->thumb('fire.jpg', 400, 400, 'th3_');  //返回新的文件名
//对图片进行加水印
echo $image->addWatermark('fire.jpg', 'clock.gif', 1, 'wa1_'); //返回文件名,第5位置开始加
echo $image->addWatermark('fire.jpg', 'clock.gif', 2, 'wa2_'); //返回文件名,第5位置开始加
echo $image->addWatermark('fire.jpg', 'clock.gif', 3, 'wa3_'); //返回文件名,第5位置开始加
echo $image->addWatermark('fire.jpg', 'clock.gif', 4, 'wa4_'); //返回文件名,第5位置开始加
echo $image->addWatermark('fire.jpg', 'clock.gif', 5, 'wa5_'); //返回文件名,第5位置开始加
echo $image->addWatermark('fire.jpg', 'clock.gif', 6, 'wa6_'); //返回文件名,第5位置开始加
echo $image->addWatermark('fire.jpg', 'clock.gif', 7, 'wa7_'); //返回文件名,第5位置开始加
echo $image->addWatermark('fire.jpg', 'clock.gif', 8, 'wa8_'); //返回文件名,第5位置开始加
echo $image->addWatermark('fire.jpg', 'clock.gif', 9, 'wa9_'); //返回文件名,第5位置开始加
echo $image->addWatermark('fire.jpg', 'clock.gif', 0, 'wa0_'); //返回文件名,第5位置开始加


?>
